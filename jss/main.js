
$(".gwrapper").each(function() {
    var minheight = $(this).find(".gad").height();
    $(this).css({
        "min-height": minheight
    });
    $(this).find(".whitebox").css({
        "min-height": minheight
    });
});
// function sticky(){
//        $(".stick_child").stick_in_parent({
//         parent: ".stick_parent",
//         offset_top: $(".navigation").height()
//     }).on('sticky_kit:bottom', function(e) {
//         $(this).parent().css('position', 'static');
//     }).on('sticky_kit:unbottom', function(e) {
//         $(this).parent().css('position', 'relative');
//     }); 
//         if ($(window).width() < 1000) {
//         $(".stick_child").trigger("sticky_kit:detach");
//     } else {
//     }
// }
// sticky();
$(".fancybox").fancybox({
    openEffect: 'elastic',
    closeEffect: 'elastic'
});
$('.fancybox-media').fancybox({
    openEffect: 'elastic',
    closeEffect: 'elastic',
    helpers: {
        media: {}
    }
});
$(".rating_output").each(function() {
    var $this = $(this);
    $this.rateYo({
        starWidth: "16px",
        rating: $this.data('value'),
        readOnly: true,
        normalFill: "rgba(0,0,0,0.1)",
        ratedFill: "#fc0"
    });
});
$(".rating_input").rateYo({
    starWidth: "18px",
    normalFill: "#ccc",
    ratedFill: "#fc0"
});
// $.ajax({
//     type: 'GET',
//     url: 'https://geoip-db.com/json/',
//     dataType: 'json',
//     success: function(data) {}
// });
$(".services_list").each(function() {
    var $this = $(this);
    var $limit = $this.data('show');
    $.ajax({
        type: 'GET',
        url: 'categories.json',
        dataType: 'json',
        success: function(data) {
            if ($limit == 'all') {
                for (i = 0; i < data.length; i++) {
                    $this.prepend('\
                <li>\
                <a href="listing.php">\
                <i style="background:url(images/icons/' + data[i].category.toLowerCase().replace(/ /g, "-") + '.svg)"></i>\
                <strong>' + data[i].category + '</strong>\
                </a>\
                </li>');
                }
            } else {
                for (i = 0; i < $limit; i++) {
                    $this.prepend('\
                <li>\
                <a href="listing.php">\
                <i style="background:url(images/icons/' + data[i].category.toLowerCase().replace(/ /g, "-") + '.svg)"></i>\
                <strong>' + data[i].category + '</strong>\
                </a>\
                </li>');
                }
            }
        }
    });
});


    $('.slimscroll').slimscroll({
        height: false,
        width: "100%",
        size: '4px',
        distance: '4px',
        color: '#09f',
        opacity:.8,
        alwaysVisible : true
    });

$('.mainslider').owlCarousel({
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    loop: true,
    smartSpeed: 1000,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});


$('.featured').owlCarousel({
    animateOut: 'zoomOut',
    animateIn: 'flipInX',
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    loop: true,
    smartSpeed: 1000,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
});

$('.related').owlCarousel({
    animateOut: 'zoomOut',
    animateIn: 'flipInX',
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    loop: true,
    smartSpeed: 1000,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});
$(".theme-select").selectpicker({
    width: "100%",
    dropupAuto: false,
    template: {
        caret: '<span class="fa fa-angle-down"></span>'
    }
});
// cache container
var $container = $('.gallery');
// initialize isotope
$container.isotope({
    // options...
    animationEngine: 'best-available',
    itemSelector: '.isotope-item'
});
$('.products_filter a').on('click', function() {
    var selector = $(this).data('filter');
    $('.products_filter a').parent().removeClass('current');
    $(this).parent().addClass('current');
    $container.isotope({
        filter: selector
    });
});
$(function() {
    $('.btn,.arrowDown,.products_filter a').materialripple();
});
$(function() {
    $('.scrollToLink').bind('click', function(event) {
        var $anchor = $(this);
        $('a').each(function() {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - $(".theme-header").height()
        }, 1500, 'easeInOutCubic');
        event.preventDefault();
    });
});
$('.counter').counterUp();
$(".hamburger").click(function() {
    $(this).toggleClass("is-active");
});

var owl = $('#feedback');
owl.owlCarousel({
    items: 2,
    loop: true,
    autoHeight: true,
    smartSpeed: 800,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause: true,
    activeClass: 'active',
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 2,
        },
        1000: {
            items: 2,
        }
    }
});